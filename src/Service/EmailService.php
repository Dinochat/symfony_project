<?php
namespace App\Service;
use Doctrine\ORM\EntityManagerInterface;
class EmailService
{
    protected $mailer;
    private $templating;
    private $em;
    public function __construct(\Swift_Mailer $mailer,\Twig_Environment $templating,EntityManagerInterface $em)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->em = $em;
    }
    public function sendContactMail($subject, $from, $to, $args) {
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->renderContactTemplate($args),
                'text/html'
            )
        ;
        $this->mailer->send($message);
    }
    public function renderContactTemplate($args)
    {
        return $this->templating->render(
            'default/contactMail.html.twig',
            $args
        );
    }
}