<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191104110046 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64944973C78');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6494A214576');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64926D6BDFF');
        $this->addSql('DROP TABLE billet');
        $this->addSql('DROP TABLE billet_exposant');
        $this->addSql('DROP TABLE billet_visiteur');
        $this->addSql('DROP INDEX UNIQ_8D93D64944973C78 ON user');
        $this->addSql('DROP INDEX UNIQ_8D93D6494A214576 ON user');
        $this->addSql('DROP INDEX UNIQ_8D93D64926D6BDFF ON user');
        $this->addSql('ALTER TABLE user DROP billet_visiteur_id, DROP billet_exposant_id, DROP billet_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE billet (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, price DOUBLE PRECISION NOT NULL, date_evenement DATETIME NOT NULL, emplacement VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, type_billet VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE billet_exposant (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, price DOUBLE PRECISION NOT NULL, date_evenement DATETIME NOT NULL, emplacement VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE billet_visiteur (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, price DOUBLE PRECISION NOT NULL, date_evenement DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user ADD billet_visiteur_id INT DEFAULT NULL, ADD billet_exposant_id INT DEFAULT NULL, ADD billet_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64926D6BDFF FOREIGN KEY (billet_visiteur_id) REFERENCES billet_visiteur (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64944973C78 FOREIGN KEY (billet_id) REFERENCES billet (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6494A214576 FOREIGN KEY (billet_exposant_id) REFERENCES billet_exposant (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64944973C78 ON user (billet_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6494A214576 ON user (billet_exposant_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64926D6BDFF ON user (billet_visiteur_id)');
    }
}
