<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\MdpFormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Form\UserFormType;
use App\Form\ContactFormType;
use App\Service\EmailService;

class ConventionController extends AbstractController
{
    /**
     * @Route("/InfoPratique", name="InfoPratique")
    */
    public function InfoPratique()
    {
        return $this->render('info/InfoPratique.html.twig');
    }

     /**
     * @Route("/Tarif", name="Tarif")
    */
    public function Tarif()
    {
        return $this->render('info/Tarif.html.twig');
    }

    /**
     * @Route("/Exposants", name="Exposants")
    */
    public function Exposants(UserRepository $userRepository): Response
    {
        return $this->render('programme/Exposants.html.twig', [
            'users' => $userRepository->findExposant($userRepository),
        ]);
    }

    /**
     * @Route("/Invites", name="Invites")
    */
    public function Invites()
    {
        return $this->render('programme/Invites.html.twig');
    }

    /**
     * @Route("/ActivitesAnimations", name="ActivitesAnimations")
    */
    public function ActivitesAnimations()
    {
        return $this->render('programme/ActivitesAnimations.html.twig');
    }

    /**
     * @Route("/Karaoke", name="Karaoke")
    */
    public function Karaoke()
    {
        return $this->render('programme/Karaoke.html.twig');
    }

    /**
     * @Route("/ConcoursCosplay", name="ConcoursCosplay")
    */
    public function ConcoursCosplay()
    {
        return $this->render('programme/ConcoursCosplay.html.twig');
    }

    /**
     * @Route("/Quizz", name="Quizz")
    */
    public function Quizz()
    {
        return $this->render('programme/Quizz.html.twig');
    }

     /**
     * @Route("/AntoineDaniel", name="AntoineDaniel")
    */
    public function AntoineDaniel()
    {
        return $this->render('programme/AntoineDaniel.html.twig');
    }

       /**
     * @Route("/Cosplayers", name="Cosplayers")
    */
    public function Cosplayers()
    {
        return $this->render('programme/Cosplayers.html.twig');
    }

       /**
     * @Route("/JDG", name="JDG")
    */
    public function JDG()
    {
        return $this->render('programme/JDG.html.twig');
    }

       /**
     * @Route("/KillingStalking", name="KillingStalking")
    */
    public function KillingStalking()
    {
        return $this->render('programme/KillingStalking.html.twig');
    }

       /**
     * @Route("/LeGrandJD", name="LeGrandJD")
    */
    public function LeGrandJD()
    {
        return $this->render('programme/LeGrandJD.html.twig');
    }

       /**
     * @Route("/RireJaune", name="RireJaune")
    */
    public function RireJaune()
    {
        return $this->render('programme/RireJaune.html.twig');
    }

    /**
     * @Route("/moncompte", name="moncompte")
    */
    public function MonCompte(Request $request)
    {
       // $usersRepo = $userRepository->findMailMdp($userRepository);
        $mail= $this->getUser();

      //  $ancientMotDePasse = $mail->getPassword(); //il est bien recup

        //---
        
        $form = $this->createForm(MdpFormType::class, $mail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $mail->setPassword(
                $passwordEncoder->encodePassword(
                    $form->get('plainPassword')->getData()
                )
            );
        }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mail);
            $entityManager->flush();

            // do anything else you need here, like send an email

        //----

        return $this->render('user/MonCompte.html.twig', [
            'mymail' => $mail,
            'registrationForm' => $form->createview()
        ]);
    }

    /**
     * @Route("/CGU", name="CGU")
    */
    public function CGU()
    {
        return $this->render('/default/CGU.html.twig');
    }

        /**
     * @Route("/contact", name="contact")
    */
    public function contact()
    {
        $form = $this->createForm(ContactFormType::class);          
        return $this->render('/default/contact.html.twig', [ 'contactForm' => $form->createview()]);
    }

    /**
     * @Route("/contactMail", name="contactMail")
    */

    public function contactMail(Request $request, EmailService $emailService)
    {
        $emailService->sendRegistrationMail('Welcome', 'noreply@localhost', $this->getUser()->getEmail(), ['user' => $this->getUser()]);
        return $this->render('default/contactMail.html.twig', [
            'vars' => $emailService
        ]);
    }

}
